﻿using System;
using System.Text;

namespace DeviceRegister
{
    internal static class EnduraBase64
    {
        public static string Encode(string value)
        {
            return Encode(Encoding.ASCII.GetBytes(value));
        }

        public static string Encode(byte[] value)
        {
            return Encode(value, 0, value.Length);
        }

        public static string Encode(byte[] value, int offset, int length)
        {
            string base64 = Convert.ToBase64String(value, offset, length);

            if (!base64.EndsWith("="))
                return base64 + "=";

            return base64;
        }
    }
}

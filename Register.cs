﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceRegister
{
    public class Register
    {
        private SmCredentials _credentials;
        private UserAndRoleHelper _userAndRole;
        private int _loginId;

        static Register()
        {
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        public Register(SmCredentials credentials)
        {
            _credentials = credentials;
        }

        public bool Open()
        {
            bool success = true;
            _userAndRole = new UserAndRoleHelper(_credentials.Ip, _credentials.Port, 1);
            _loginId = _userAndRole.UserLogin(_credentials.Username, _credentials.Password);

            if (_loginId == 0)
            {
                _userAndRole = null;
                success = false;
            }
            return success;
        }

        public Dictionary<string, DeviceInfo> GetDevices(DeviceType[] types)
        {
            if (_loginId == 0)
                throw new MethodAccessException("You must first login");

            RegisterHelper registerHelper = new RegisterHelper(_credentials.Ip, _credentials.Port, 1);
            int seqId = 0;
            Dictionary<string, DeviceInfo> devices = new Dictionary<string, DeviceInfo>();
            
            registerHelper.GetDevices(devices, types, _loginId, ref seqId);
            
            return devices;
        }

        public void Close()
        {
            if(_userAndRole != null)
                _userAndRole.UserLogout(_loginId);
        }
    }
}

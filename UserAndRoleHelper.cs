﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceRegister
{
    internal class UserAndRoleHelper
    {
        private string _ipAddress;
        private ushort _port;
        private ushort _serviceId;
        private UserAndRoleNs.UserAndRole _userAndRoleProxy;

        public UserAndRoleHelper(string ipAddress, ushort port, ushort serviceId)
        {
            _ipAddress = ipAddress;
            _port = port;
            _serviceId = serviceId;
            _userAndRoleProxy = new UserAndRoleNs.UserAndRole();
            _userAndRoleProxy.Url = ControlUrl;
            _userAndRoleProxy.CustomHeader = new EVHeader();
        }

        private string ControlUrl
        {
            get
            {
                return String.Format(@"http://{0}:{1}/control/UserAndRole-{2}", _ipAddress, _port, _serviceId);
            }
        }

        public int UserLogin(string user, string password)
        {
            UserAndRoleNs.User uarUser = new UserAndRoleNs.User();
            uarUser.dbId = 0;
            uarUser.name = user;

            UserAndRoleNs.Pswd uarPassword = new UserAndRoleNs.Pswd();
            uarPassword.type = UserAndRoleNs.PASSWORDTYPES.Item1;
            uarPassword.data = EnduraBase64.Encode(password);

            try
            {
                return _userAndRoleProxy.UserLogin(uarUser, uarPassword);
            }
            catch (System.Web.Services.Protocols.SoapException)
            {
                return 0;
            }
            catch (System.Net.WebException)
            {
                return 0;
            }
        }
        
        public void UserLogout(int loginId)
        {
            if (loginId != 0)
                _userAndRoleProxy.UserLogout(loginId);
        }
    }
}

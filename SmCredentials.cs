﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceRegister
{
    public class SmCredentials
    {
        public string Ip { get; set; }
        public ushort Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

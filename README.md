DeviceRegister
====================

Summary
---------------------
The DeviceRegister project is a good way to get devices from an Endura system manager via the Pelco API only.

Features
---------------------
* Get devices from a system manager exclusively by using SOAP

Building
---------------------
1. git clone https://bitbucket.org/blackey02/deviceregister.git
2. Open the Visual Studio project file
3. Build and run

Acknowledgements
---------------------
* Rob Schafer
* [Pelco](http://pdn.pelco.com)